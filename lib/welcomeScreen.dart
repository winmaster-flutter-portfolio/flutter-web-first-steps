import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter_web_app/resources.dart';
import 'package:transparent_image/transparent_image.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
            TypewriterAnimatedTextKit(
              speed: Duration(milliseconds: 200),
              pause: Duration(milliseconds: 1000),
              text: [
                ResString().get('welcome'),
                ModalRoute.of(context).settings.arguments.toString()
              ],
              textStyle: Theme.of(context).textTheme.headline2,
              textAlign: TextAlign.start,
            ),
              FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: ResString().get('url')),
            ]
        ),
      ),
    );
  }
}