import 'dart:ui';

class ResString{
  var data = {
    'url' : 'https://picsum.photos/200',
    'welcome' : 'Welcome',
    'signUp' : 'Sign up',
    'description' :'Simple Flutter multi-platform dummy-app \n to test new Flutter 2.0 potential',
    'firstName' : 'First name',
    'lastName' : 'Last name',
    'username' : 'Username'
  };

  String get(String key){
    return data[key];
  }
}