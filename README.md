# flutter_web_app

A new Flutter web application.

In tribute to the new Flutter 2.0 release and official web development support, this is my first attempt at the multi-platform and multi-idiom app in this framework.

:rocket: See it in action [here](https://winmaster-flutter-portfolio.gitlab.io/flutter-web-first-steps/)
 on Gitlab page.

:dart: or download apk file [here](/apk/app.apk) 

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
